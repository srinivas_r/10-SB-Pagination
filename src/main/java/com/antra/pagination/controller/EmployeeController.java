package com.antra.pagination.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.antra.pagination.model.PageDetails;
import com.antra.pagination.service.EmployeeService;

@Controller
public class EmployeeController {
	
	@Autowired
	EmployeeService  employeeService;
	
	@GetMapping( value = "/employees")
	public String getEmployee(@RequestParam(value="pageNumber", defaultValue = "0")Integer pageNumber, Model model) {
		PageDetails pageDetails = employeeService.fetchEmployeesOfPage(pageNumber);
		model.addAttribute("pageDetails", pageDetails);
		return "display";
	}
}
