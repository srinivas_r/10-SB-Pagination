package com.antra.pagination.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.antra.pagination.entity.EmployeeEntity;
import com.antra.pagination.model.Employee;
import com.antra.pagination.model.PageDetails;
import com.antra.pagination.repository.EmployeeEntityRepository;
import com.antra.pagination.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	EmployeeEntityRepository repository;

	@Override
	public PageDetails fetchEmployeesOfPage(Integer pageNumber) {
		
		Pageable pageable = PageRequest.of(pageNumber, 5);
		
		Page page = repository.findAll(pageable);
		
		List<EmployeeEntity> lstEntities = page.getContent();
		List<Employee> lstEmps = new ArrayList<>();
		
		lstEntities.forEach(entity -> {
			Employee emp = new Employee();
			BeanUtils.copyProperties(entity, emp);
			lstEmps.add(emp);
		});
		
		PageDetails pageDetails = new PageDetails();
		pageDetails.setLstEmps(lstEmps);
		pageDetails.setCurrentPageNumber(page.getNumber());
		pageDetails.setNextPageExists(page.hasNext());
		pageDetails.setPreviousPageExists(page.hasPrevious());
		
		return pageDetails;
	}

}
