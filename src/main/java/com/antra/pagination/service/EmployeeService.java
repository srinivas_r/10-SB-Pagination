package com.antra.pagination.service;

import com.antra.pagination.model.PageDetails;

public interface EmployeeService {
	
	PageDetails fetchEmployeesOfPage(Integer pageNumber);

}
