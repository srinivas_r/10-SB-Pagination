package com.antra.pagination.model;

import java.util.List;

import lombok.Data;

@Data
public class PageDetails {
	private List<Employee> lstEmps;
	private Integer currentPageNumber;
	private boolean isNextPageExists;
	private boolean isPreviousPageExists;

}
