package com.antra.pagination.model;

import lombok.Data;

@Data
public class Employee {
	private Integer empno;
	private String ename;
	private Double sal;
	private Integer deptno;

}
